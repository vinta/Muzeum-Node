#!/bin/sh

set -e

go get github.com/bitmark-inc/bitmark-trade

mkdir -p /var/lib/bitmark/

bitmark-trade -conf=/go/src/app/dockyard/bitmark-trade.conf
