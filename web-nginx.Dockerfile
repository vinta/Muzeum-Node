FROM nginx:1.11.9

RUN rm /etc/nginx/conf.d/default.conf
ADD muzeum-web-node-docker.app /etc/nginx/conf.d/muzeum-web-node.conf
