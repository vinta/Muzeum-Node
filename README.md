# Muzeum Node

Prerequisites:

- [Docker](https://www.docker.com/community-edition)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Development

Start all services.

```console
$ git clone https://github.com/KKFARM-inc/Muzeum-Web-Node.git
$ git clone https://github.com/KKFARM-inc/Muzeum-Node.git
$ cd Muzeum-Node
$ make up
```

### Muzeum Crawlers

```console
$ make get_indices
$ make run_crawlers
$ make ipfs_add
$ make init_mappings
$ make run_indexers
```

### Muzeum Web Node

Only needed for the first time.

```console
# create a Bitmark account
$ docker-compose exec bitmark curl -X POST http://127.0.0.1:9080/account
$ sed -i '' 's/BITMARK_ACCOUNT=/BITMARK_ACCOUNT=YOUR_BITMARK_ACCOUNT/g' ../Muzeum-Web-Node/.env

# generate an Laravel app key
$ docker-compose exec app php artisan key:generate
$ docker-compose exec app php artisan migrate
$ docker-compose up --force-recreate
```

Open Muzeum Web Node.

```console
$ open http://127.0.0.1:8000/
```

## Deployment

- Assume the target host machine runs Ubuntu 16.04
- And enable SSH keys login which means you could `ssh` without password

Deploy both Muzeum Node and Muzeum Web Node on the same machine.

```console
$ pip install ansible

$ cd playbooks
$ sed -i '' 's/ansible_host=1.2.3.4/ansible_host=YOUR_HOST_IP/g' hosts
$ ansible-playbook site.yml -v
```

```console
$ ssh YOUR_USERNAME@YOUR_HOST_IP

$ cd /var/www/Muzeum-Web-Node
$ php artisan key:generate
$ php artisan migrate

$ curl -X POST http://127.0.0.1:8080/account
$ sed -i '' 's/BITMARK_ACCOUNT=/BITMARK_ACCOUNT=YOUR_BITMARK_ACCOUNT/g' .env

# show logs
$ tail -fn 500 /var/log/nginx/muzeum-web-node.app-error.log
```

Open Muzeum Web Node.

```console
$ open http://YOUR_HOST_IP/
```

## TODO

### Deployment

- [ ] create a Bitmark account automatically
- [ ] support multi-node deployment
