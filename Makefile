# MAKEFLAGS += --jobs
INDICES = music_group music_album music_recording music_composition

.PHONY: prepare up stop ipfs ipfs_add ipfs_gc get_indices crawler run_crawlers init_mappings run_indexers all deploy $(INDICES)

prepare:
	cd ../Muzeum-Web-Node && cp ../Muzeum-Node/web-laravel.Dockerfile laravel.Dockerfile
	cd ../Muzeum-Web-Node && cp ../Muzeum-Node/web-nginx.Dockerfile nginx.Dockerfile
	cd ../Muzeum-Web-Node && cp ../Muzeum-Node/web-nginx-server.conf muzeum-web-node-docker.app
	[ -f ../Muzeum-Web-Node/.env ] || cd ../Muzeum-Web-Node && cp ../Muzeum-Node/web.env.template .env
	rm .env

up: prepare
	docker-compose up

stop:
	docker-compose stop

build: prepare
	docker-compose build

ipfs:
	docker-compose exec ipfs sh

ipfs_add:
	docker-compose exec ipfs ipfs add -r /export/soundscape/music_group
	docker-compose exec ipfs ipfs add -r /export/soundscape/music_album
	docker-compose exec ipfs ipfs add -r /export/soundscape/music_recording
	docker-compose exec ipfs ipfs add -r /export/soundscape/music_composition

ipfs_gc:
	docker-compose exec ipfs ipfs pin ls --type recursive | cut -d' ' -f1 | xargs -n1 ipfs pin rm
	docker-compose exec ipfs ipfs repo gc

# $(INDICES):
# 	docker-compose exec ipfs mkdir -p /export/soundscape/$@
# 	docker-compose exec ipfs wget https://ipfs.io/ipns/ipfs.soundscape.net/$@/index.json -O "/export/soundscape/$@/index.json"

# get_indices: $(INDICES)

get_indices:
	docker-compose exec ipfs mkdir -p /export/soundscape/music_group
	docker-compose exec ipfs wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_group/index.json -O "/export/soundscape/music_group/index.json"
	docker-compose exec ipfs mkdir -p /export/soundscape/music_album
	docker-compose exec ipfs wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_album/index.json -O "/export/soundscape/music_album/index.json"
	docker-compose exec ipfs mkdir -p /export/soundscape/music_recording
	docker-compose exec ipfs wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_recording/index.json -O "/export/soundscape/music_recording/index.json"
	docker-compose exec ipfs mkdir -p /export/soundscape/music_composition
	docker-compose exec ipfs wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_composition/index.json -O "/export/soundscape/music_composition/index.json"

crawler:
	docker-compose exec crawler sh

run_crawlers:
	docker-compose exec crawler sh -c "cd /app/crawlers && python crawl_soundscape.py music_group"
	docker-compose exec crawler sh -c "cd /app/crawlers && python crawl_soundscape.py music_album"
	docker-compose exec crawler sh -c "cd /app/crawlers && python crawl_soundscape.py music_composition"
	docker-compose exec crawler sh -c "cd /app/crawlers && python crawl_soundscape.py music_recording"

init_mappings:
	docker-compose exec crawler sh -c "cd /app/crawlers && python mappings.py"

run_indexers:
	docker-compose exec crawler sh -c "cd /app/crawlers && python index_soundscape.py music_group"
	docker-compose exec crawler sh -c "cd /app/crawlers && python index_soundscape.py music_album"
	docker-compose exec crawler sh -c "cd /app/crawlers && python index_soundscape.py music_composition"
	docker-compose exec crawler sh -c "cd /app/crawlers && python index_soundscape.py music_recording"

all: get_indices run_crawlers ipfs_add init_mappings run_indexers

deploy:
	cd playbooks && ansible-playbook site.yml -v
