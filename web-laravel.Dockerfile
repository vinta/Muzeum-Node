FROM php:7.1.17-fpm-jessie

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        mysql-client
