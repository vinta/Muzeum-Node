# coding: utf-8

from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
import argparse
import json
import logging
import os

from elasticsearch import Elasticsearch
from elasticsearch.exceptions import TransportError


file_handler = logging.FileHandler('index_soundscape.log')
file_handler.setLevel(logging.DEBUG)
logger = logging.getLogger('index_soundscape')
logger.addHandler(file_handler)

es = Elasticsearch([os.environ.get('ES_HOST', 'elasticsearch'), ])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('category', choices=['music_group', 'music_album', 'music_composition', 'music_recording'], action='store', type=str)

    args = parser.parse_args()
    category = args.category

    base_dir = Path('/export/soundscape/')
    category_dir = base_dir / category
    print(f'scan: {category_dir}')

    maps = {
        'music_group': {'index': 'muzeum_music_group', 'doc_type': 'MusicGroup'},
        'music_album': {'index': 'muzeum_music_album', 'doc_type': 'MusicAlbum'},
        'music_composition': {'index': 'muzeum_music_composition', 'doc_type': 'MusicComposition'},
        'music_recording': {'index': 'muzeum_music_recording', 'doc_type': 'MusicRecording'},
    }

    def import_to_elasticsearch(file_path):
        if file_path.name.endswith('index.json'):
            return
        print(f'import_to_elasticsearch: {file_path}')
        with open(file_path) as fp:
            try:
                data = json.loads(fp.read())
            except json.JSONDecodeError as exc:
                logger.error(f'{exc}, file_path: {file_path}')
                return
        try:
            es.index(index=maps[category]['index'], doc_type=maps[category]['doc_type'], id=data['did'], body=data)
        except (KeyError, TransportError) as exc:
            logger.error(f'{exc}, data: {data}')
            return

    with ThreadPoolExecutor(max_workers=4) as executor:
        for result in executor.map(import_to_elasticsearch, category_dir.glob('*.json')):
            pass
