#!/bin/sh

# mkdir -p /export/soundscape/music_group
# wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_group/index.json -O "/export/soundscape/music_group/index.json"
# mkdir -p /export/soundscape/music_album
# wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_album/index.json -O "/export/soundscape/music_album/index.json"
# mkdir -p /export/soundscape/music_recording
# wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_recording/index.json -O "/export/soundscape/music_recording/index.json"
# mkdir -p /export/soundscape/music_composition
# wget https://ipfs.muzeum.pro/ipns/ipfs.soundscape.net/music_composition/index.json -O "/export/soundscape/music_composition/index.json"

# python3 crawl_soundscape.py music_group
# python3 crawl_soundscape.py music_album
# python3 crawl_soundscape.py music_composition
# python3 crawl_soundscape.py music_recording

python3 mappings.py

python3 index_soundscape.py music_group
python3 index_soundscape.py music_album
python3 index_soundscape.py music_composition
python3 index_soundscape.py music_recording

ipfs add -r /export/soundscape/music_group
ipfs add -r /export/soundscape/music_album
ipfs add -r /export/soundscape/music_recording
ipfs add -r /export/soundscape/music_composition
