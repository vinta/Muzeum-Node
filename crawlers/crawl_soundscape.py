# coding: utf-8

from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
import argparse
import json
import logging
import subprocess

import requests


file_handler = logging.FileHandler('crawl_soundscape.log')
file_handler.setLevel(logging.DEBUG)
logger = logging.getLogger('crawl_soundscape')
logger.addHandler(file_handler)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('category', choices=['music_group', 'music_album', 'music_composition', 'music_recording'], action='store', type=str)

    args = parser.parse_args()

    # TODO: should we clear old .json files before fetching new ones?
    base_dir = Path('/export/soundscape/')
    category_dir = base_dir / args.category
    index_file = category_dir / 'index.json'
    print(f'iterate: {index_file}')

    with open(index_file, 'r') as fp:
        index_entities = json.loads(fp.read())

    def save_to_disk(base_dir, entity):
        def get_raw_json(url):
            try:
                res = requests.get(url)
            except (requests.exceptions.ConnectionError, requests.exceptions.SSLError) as exc:
                logger.error(f'{exc}, url: {url}')
            else:
                return res.content
            return

        urls = [
            entity['ipns_url'].replace('https://ipfs.io/', 'https://ipfs.muzeum.pro/'),
            entity['ipns_url'],
            entity['url'],
        ]
        for url in urls:
            print(f'save_to_disk: {url}')
            raw_json = get_raw_json(url)
            if raw_json:
                try:
                    preview_data = json.loads(raw_json)
                    preview_data['did']
                except json.JSONDecodeError as exc:
                    logger.error(f'{exc}, url: {url}, raw_json: {raw_json}')
                except KeyError as exc:
                    logger.error(f'{exc}, url: {url}, preview_data: {preview_data}')
                else:
                    file_path = entity['ipns_url'].replace('https://ipfs.io/ipns/ipfs.soundscape.net', str(base_dir))
                    with open(file_path, 'wb') as fp:
                        fp.write(raw_json)
                    print(f'save: {file_path}')
                    return

    with ThreadPoolExecutor(max_workers=4) as executor:
        for result in executor.map(lambda x: save_to_disk(base_dir, x), index_entities):
            pass
