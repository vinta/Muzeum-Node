# coding: utf-8

import os

from elasticsearch_dsl import Index, DocType
from elasticsearch_dsl import Date, InnerDoc, Integer, Keyword, Nested, Object, Text
from elasticsearch_dsl import token_filter, analyzer
from elasticsearch_dsl.connections import connections


connections.create_connection(hosts=[os.environ.get('ES_HOST', 'elasticsearch'), ])

jieba_stop = token_filter('jieba_stop', type='stop', stopwords_path='/usr/share/elasticsearch/plugins/jieba/dic/stopwords.txt')
jieba_analyzer = analyzer('jieba_analyzer',
    char_filter=['html_strip'],
    tokenizer='jieba_index',
    filter=['asciifolding', 'lowercase', jieba_stop]
)

muzeum_music_group_index = Index('muzeum_music_group')
muzeum_music_group_index.settings(number_of_shards=1, number_of_replicas=1)
muzeum_music_group_index.analyzer(jieba_analyzer)

muzeum_music_album_index = Index('muzeum_music_album')
muzeum_music_album_index.settings(number_of_shards=1, number_of_replicas=1)
muzeum_music_album_index.analyzer(jieba_analyzer)

muzeum_music_recording_index = Index('muzeum_music_recording')
muzeum_music_recording_index.settings(number_of_shards=1, number_of_replicas=1)
muzeum_music_recording_index.analyzer(jieba_analyzer)

muzeum_music_composition_index = Index('muzeum_music_composition')
muzeum_music_composition_index.settings(number_of_shards=1, number_of_replicas=1)
muzeum_music_composition_index.analyzer(jieba_analyzer)


class InnerArtist(InnerDoc):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    name = Text(fields={'raw': Keyword()})
    alternateName = Text(fields={'raw': Keyword()})


class InnerAlbum(InnerDoc):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    name = Text(fields={'raw': Keyword()})


class InnerRecording(InnerDoc):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    name = Text(fields={'raw': Keyword()})


class InnerAudio(InnerDoc):
    bitrate = Integer()
    uploadDate = Date(default_timezone='Asia/Taipei')


class InnerComposition(InnerDoc):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    name = Text(fields={'raw': Keyword()})


class InnerPerson(InnerDoc):
    name = Text(fields={'raw': Keyword()})
    op = Text(fields={'raw': Keyword()})
    sp = Text(fields={'raw': Keyword()})
    ratio = Text(fields={'raw': Keyword()})
    ethereum_address = Keyword()


@muzeum_music_group_index.doc_type
class MusicGroup(DocType):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    name = Text(fields={'raw': Keyword()})
    alternateName = Text(fields={'raw': Keyword()})
    image = Keyword()
    album = Nested(InnerAlbum)
    track = Nested(InnerRecording)

    class Meta:
        doc_type = 'MusicGroup'


@muzeum_music_album_index.doc_type
class MusicAlbum(DocType):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    upcCode = Keyword()
    albumReleaseType = Keyword()
    name = Text(fields={'raw': Keyword()})
    numTracks = Integer()
    datePublished = Date(default_timezone='Asia/Taipei')
    genre = Keyword()
    headline = Text(fields={'raw': Keyword()})
    publisher = Text(fields={'raw': Keyword()})
    thumbnailUrl = Keyword()
    description = Text()
    p_line = Text(fields={'raw': Keyword()})
    c_line = Text(fields={'raw': Keyword()})
    explicity = Keyword()
    smart_contract_hash = Keyword()
    byArtist = Nested(InnerArtist)
    track = Nested(InnerRecording)

    class Meta:
        doc_type = 'MusicAlbum'


@muzeum_music_recording_index.doc_type
class MusicRecording(DocType):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    isrcCode = Keyword()
    name = Text(fields={'raw': Keyword()})
    duration = Keyword()
    datePublished = Date(default_timezone='Asia/Taipei')
    genre = Keyword()
    position = Integer()
    headline = Text(fields={'raw': Keyword()})
    publisher = Text(fields={'raw': Keyword()})
    thumbnailUrl = Keyword()
    description = Text()
    p_line = Text(fields={'raw': Keyword()})
    c_line = Text(fields={'raw': Keyword()})
    explicity = Keyword()
    smart_contract_hash = Keyword()
    byArtist = Nested(InnerArtist)
    inAlbum = Object(InnerAlbum)
    recordingOf = Object(InnerRecording)
    recording_share = Nested(InnerPerson)
    audio = Object(InnerAudio)

    class Meta:
        doc_type = 'MusicRecording'


@muzeum_music_composition_index.doc_type
class MusicComposition(DocType):
    did = Keyword()
    ipns_url = Keyword()
    url = Keyword()
    iswcCode = Keyword()
    name = Text(fields={'raw': Keyword()})
    lyrics = Text()
    explicity = Keyword()
    smart_contract_hash = Keyword()
    inAlbum = Object(InnerAlbum)
    recordedAs = Object(InnerRecording)
    composer = Nested(InnerPerson)
    lyricist = Nested(InnerPerson)

    class Meta:
        doc_type = 'MusicComposition'


muzeum_music_group_index.create(ignore=400)
muzeum_music_album_index.create(ignore=400)
muzeum_music_recording_index.create(ignore=400)
muzeum_music_composition_index.create(ignore=400)
